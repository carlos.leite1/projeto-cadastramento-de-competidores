// load dependencies
const express = require('express');
const formController = require('./controllers/form-controller.js');
const path = require('path');
const bodyParser = require('body-parser');
const Joi = require('joi');

// create new app
const app = express();
const PORT = process.env.PORT || '3000';

app.set('view engine', 'ejs');
app.use(express.static(path.join(__dirname, '/public')));
app.use(bodyParser.urlencoded({ extended: true }));


app.listen(`${PORT}`, () => {
    console.log(`Working at port ${PORT}`);
});

app.get('/', (req, res, next) => {
    res.render('index', {});
});

app.get('/about', (req, res, next) => {
    res.render('about', {});
});

app.get('/signup', formController.formGet);

app.post('/signup', formController.formPost);

//heroku

