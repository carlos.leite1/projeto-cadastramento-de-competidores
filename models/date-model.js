function formatDate(oldDate) {
    var datePart = oldDate.match(/\d+/g),
        year = datePart[0],
        month = datePart[1], 
        day = datePart[2];

    return day + '/' + month + '/' + year;
}


module.exports = {
    formatDate,
}