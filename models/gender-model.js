const data = [
    {
      id: '1',
      description: 'Masculino'
    },
    {
      id: '2',
      description: 'Feminino'
    }
  ]
  
  const getGenderById = (id) => {
  
    const answer = data.find((item) => {
  
      return item.id === id
  
    });
  
    return answer
  
  }
  
  const getAllGender = () => {
    return data;
  }
  
  module.exports = {
    getAllGender,
    getGenderById
  }