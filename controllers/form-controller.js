const fs = require('fs');
const ejs = require('ejs');
const html_to_pdf = require('html-pdf-node');

const genderModel = require('../models/gender-model.js');
const gameModel = require('../models/game-model.js');
const dateModel = require('../models/date-model.js');


const createGetFormViewModel = () => {

  //Sexo
  const resultModel = genderModel.getAllGender();
  const genderItemsViewModel = resultModel.map((item) => {
    return {
      value: item.id,
      label: item.description
    }
  });

  //Jogo
  const resultGame = gameModel.getAllGame()
  const gameItemsViewModel = resultGame.map((item) => {
    return {
      value: item.id,
      label: `${item.description}`,
    }
  })

  const getViewModel = {
    gender: genderItemsViewModel,
    game: gameItemsViewModel,
  }

  return getViewModel

}

const handlerGetForm = (req, res, next) => {




  res.render('signup-form', createGetFormViewModel());

}

const handlerPostForm = (req, res, next) => {


  const body = req.body;

  const genderResult = genderModel.getGenderById(body.gender);
  const gameResult = gameModel.getGameById(body.game);
  const dataResult = dateModel.formatDate(body.age);

  const viewModel = {
    name: body.name,
    nickname: body.nickname,
    email: body.email,
    age: dataResult,
    rg: body.rg,
    gender: genderResult.description,
    game: gameResult.description,
    gameImage: gameResult.imageBase64,
    //placeholder para gerar um id aleatório
    competitorId: Date.now()
  }

  var htmlText = fs.readFileSync('./views/signup-pdf.ejs', 'utf8');
  var htmlReady = ejs.render(htmlText, viewModel);

  // transformar em PDF
  let file = {
    content: htmlReady
  };
  let options = {
    format: 'A4',
    printBackground: true
  };
  html_to_pdf.generatePdf(file, options)
    .then(pdfBuffer => {

      res.contentType("application/pdf");
      res.send(pdfBuffer);

    });

}

module.exports = {
  formGet: handlerGetForm,
  formPost: handlerPostForm,
  createGetFormViewModel
}